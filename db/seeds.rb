# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Diagnostic.delete_all
open("/vagrant/Individual_Project/Dermatologist1/codes.csv") do |code|
  code.read.each_line do |code|
    code, description, fee = code.chomp.split(",")
    Diagnostic.create!( code: code, description: description, fee: fee)
  end
end

