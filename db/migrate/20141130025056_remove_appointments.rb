class RemoveAppointments < ActiveRecord::Migration
  def change
    remove_column :appointments, :hour, :string
    remove_column :appointments, :employee_id, :integer
  end
end
