class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.float :total
      t.date :date
      t.integer :employee_id
      t.integer :appointment_id

      t.timestamps
    end
  end
end
