class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :physician_id
      t.integer :patient_id
      t.string :date
      t.string :hour
      t.text :reason
      t.text :note
      t.integer :employee_id
      t.integer :diagnostic_id

      t.timestamps
    end
  end
end
