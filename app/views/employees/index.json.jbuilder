json.array!(@employees) do |employee|
  json.extract! employee, :id, :first_name, :last_name, :street_address, :city, :state, :zip_code, :email, :phone
  json.url employee_url(employee, format: :json)
end
