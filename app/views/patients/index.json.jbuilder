json.array!(@patients) do |patient|
  json.extract! patient, :id, :first_name, :last_name, :street_address, :city, :state, :zip_code, :email, :phone, :insurance_id
  json.url patient_url(patient, format: :json)
end
