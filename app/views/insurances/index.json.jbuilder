json.array!(@insurances) do |insurance|
  json.extract! insurance, :id, :name, :street_address, :city, :state, :zip_code, :phone
  json.url insurance_url(insurance, format: :json)
end
