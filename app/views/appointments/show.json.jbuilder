json.extract! @appointment, :id, :physician_id, :patient_id, :date, :hour, :reason, :note, :employee_id, :diagnostic_id, :created_at, :updated_at
