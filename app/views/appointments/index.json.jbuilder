json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :physician_id, :patient_id, :date, :hour, :reason, :note, :employee_id, :diagnostic_id
  json.url appointment_url(appointment, format: :json)
end
