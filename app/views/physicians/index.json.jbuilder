json.array!(@physicians) do |physician|
  json.extract! physician, :id, :first_name, :last_name, :street_address, :city, :state, :zip_code, :email, :phone
  json.url physician_url(physician, format: :json)
end
