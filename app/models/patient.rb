class Patient < ActiveRecord::Base
  belongs_to :insurance
  has_many :physicians, :through => :appointments
  has_many :appointments

end

